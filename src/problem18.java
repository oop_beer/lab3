import java.util.Scanner;

public class problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        int m;
        while (true) {
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            n = sc.nextInt();
            if (n == 1) {
                System.out.print("Please input number: ");
                m = sc.nextInt();
                for (int i = 1; i <= m; i++) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
            else if (n == 2) {
                System.out.print("Please input number: ");
                m = sc.nextInt();
                for (int i = 1; i <= m; i++) {
                    for (int j = 5; j > i; j--) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
else if (n == 3) {
                System.out.print("Please input number: ");
                m = sc.nextInt();
                for (int i = 0; i < m; i++) {
                    for (int j = 0; j < i; j++) {
                        System.out.print(" ");
                    }
                    for (int j = 0; j < m - i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
            else if (n == 4) {
                System.out.print("Please input number: ");
                m = sc.nextInt();
                for (int i = m; i >= 0; i--) {
                    for (int j = 0; j < m; j++) {
                        if (j >= i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }

            }
            else if (n == 5) {
                System.out.println("Bye bye!!!");
                System.exit(0);
            } else {
                System.out.println("Error: Please input number between 1-5");

            }
        }
    }
}
